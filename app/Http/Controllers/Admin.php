<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class Admin extends Controller
{
    public function index(){
        return view('admin.dashboard');
    }
    public function createProduct(Request $request){
       $product= Product::create([
            'name'=>$request->name,
            'description'=>$request->description,
            'price'=>$request->price,
            'quantity'=>$request->quantity,
            'image'=>''
        ]);
        return response($product);
    }
    /**
     * @author Muhammad Zeeshan Arif
     * @description Update Product
     */
    public function updateProduct(Request $request){
        $product_id=$request->id;
        Product::whereId($product_id)->update($request->all());

    }
}

