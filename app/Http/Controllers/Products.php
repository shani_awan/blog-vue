<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class Products extends Controller
{
    public function index(){
        return Product::getAllProducts();
    }

    /**
     * @author Muammad Zeeshan Arif
     * @description Get Single product informaton
     */
    public function getSingleProduct($product_id){
        return Product::find($product_id);
    }


}
