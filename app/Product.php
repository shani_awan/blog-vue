<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['name','description','price','quantity','image'];

    public static function getProductName($id){
        return self::whereId($id)->value('name');
    }
    public static function getAllProducts(){
        return self::all();
    }
}
