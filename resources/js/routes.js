import VueRouter from 'vue-router'
import Vue from 'vue'
import dashboard from './components/Admin/dashboard.vue'
import productCreate from './components/Admin/product/create.vue'
import categoryCreate from './components/Admin/category/create.vue'
import productLists from './components/Admin/product/lists.vue'
import productEdit from './components/Admin/product/edit.vue'
import productInfo from './components/Admin/product/info.vue'
Vue.use(VueRouter);
const router = new VueRouter({
    routes:[
        {
            path:"/",
            component:dashboard

        },
        {
            path:"/product/create",
            component:productCreate

        },
        {
            path:"/product/lists",
            component:productLists,
            children: [
                {
                    path: 'information',
                    component:productInfo,
                },
            ]

        },
        {
            path:"/product/edit/:id",
            component:productEdit,
        },
        {
            path:"/category/create",
            component:categoryCreate


        }

    ]

});
export default router