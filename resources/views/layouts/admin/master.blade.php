@include('layouts.admin.header')
<div id="app">
    <div id="layoutSidenav">
        @include('layouts.admin.left-navbar')
            <div id="layoutSidenav_content">
                @yield('content')
            </div>
    </div>
</div>
@include('layouts.admin.footer')