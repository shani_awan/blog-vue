<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//this is for the main default domain that I host my main site/CMS
Route::group(['domain' => 'blogsitetest.com'], function(){
    Route::get('/', function(){ return 'this is the index page'; });
    Route::get('/new_name', function(){ return 'this is the new page'; });
});
Route::group(['domain' => 'blogsitenew.com'], function(){
    Route::get('/', function(){ return 'this is the blog new page'; });
});

//this catches the rest of the domains and all their pages:
//Route::any('{all}', 'SitesPublicController@index')->where('all', '.*');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/get/products', 'Products@index')->name('products-fetch');
Route::get('/get/single/product/{id}', 'Products@getSingleProduct')->name('products-single');
Route::get('/test', function (){
    dd(\App\Product::getAllProducts());
})->name('products-test');

Route::group(['prefix'=>'admin'],function(){
    Route::get('/', 'Admin@index')->name('admin.index');
    Route::post('/create/product', 'Admin@createProduct')->name('admin.create.product');
    Route::post('/update/product', 'Admin@updateProduct')->name('admin.update.product');
});